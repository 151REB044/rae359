# RAE359

## Distributed systems

Studenta apl. num.: 151REB044


### Links

[Safari](https://www.safaribooksonline.com/)

[Distance Education Centre](http://tsc.edx.lv/)

[Distributed Systems in One Lesson](https://www.safaribooksonline.com/library/view/distributed-systems-in/9781491924914/)

### Videos:

*[Videolekcijas pārskats](https://youtu.be/FjL9Y5lciL0)

*[Atskats uz kursu "Komunikācijas distributīvās sistēmas"](http://www.youtube.com/watch?v=pLt5qsNMhks)

(Kopā ar [Vītālijs Devjatovskis](https://bitbucket.org/vitalijs_devjatovskis/) un [Ričards Kudojars](https://bitbucket.org/Kudojar/))